// app/routes.js
const shortid = require('shortid'); // uid generator
const sharp = require('sharp'); // image procesing
const User = require('../app/models/user');
const fs = require('fs');
module.exports = function(app, passport) {
    // show page with login and signup links
    app.get('/', function(req, res) {
        res.render('index.ejs'); // load the index.ejs file
    });

    // show the login form
    app.get('/login', function(req, res) {
        res.render('login.ejs', { message: req.flash('loginMessage') });
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        failureRedirect: '/login', // redirect back to the login page if there is an error
        failureFlash: true // allow flash messages
    }), perRoleRouter);

    // show the signup form
    app.get('/signup', function(req, res) {
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }), perRoleRouter);

    // user home page
    app.get('/profile', isLoggedIn, isBannedUser, function(req, res) {
        res.render('profile.ejs', {
            user: req.user, // get the user out of session and pass to template
            message: req.flash('uploadMessage')
        });
    });
    // logout
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });
    // admin page
    app.get('/admin', isLoggedIn, isSuperUser, function(req, res) {
        res.render('admin.ejs', {
            user: req.user // get the user out of session and pass to template
        });
    });
    // get all users for admin page
    app.get('/admin/:data', isLoggedIn, isSuperUser, function(req, res) {
        User.find({}, function(err, users) {
            res.send(users);
        });
    });
    // update user data
    app.post('/users', isLoggedIn, isSuperUser, function(req, res) {
        console.log('changed users - ' + req.body.users);
        req.body.users.forEach(user => {
            User.findByIdAndUpdate(user._id, { $set: { "local.isBanned": user.local.isBanned } }, { new: true }, function(err, updatedUser) {
                if (err) {
                    res.send({ 'message': 'Error updating users', 'error': err });
                    return handleError(err);
                }

            });
        })
        res.send({ 'message': 'Users updated!', 'users': req.body.users });
    });
    // users change they info here
    app.post('/profile', isLoggedIn, function(req, res) {
        if (!req.files) {
            req.flash('uploadMessage', 'Error while uploading');
            res.redirect('/profile');
        }
        console.log('data - ', req.body);
        // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
        if (req.files && req.files.photo !== undefined) {
            console.log('photo in request');
            // check for img folder exist
            if (!fs.existsSync('img')) {
                fs.mkdirSync('img');
            }
            let file = req.files.photo.data;
            const fileFullPath = 'img/photo-320x240' + shortid.generate() + '.jpg'
            sharp(file)
                .resize(320, 240)
                .toFile(fileFullPath, (err, info) => {
                    if (err) {
                        console.log('Saving file error', err);
                        req.flash('uploadMessage', 'Error while saving photo');
                        res.redirect('/profile');
                    }
                    User.findByIdAndUpdate(req.user.id, { $set: { "local.photo": fileFullPath, "local.name": req.body.name, "local.age": req.body.age } }, { new: false }, function(err, oldUser) {
                        if (err) return handleError(err);
                        req.flash('uploadMessage', 'Your info updated');
                        // remove old user photo from server if exist
                        if (oldUser.local.photo) {
                            console.log('old photo detected at = ' + oldUser.local.photo);
                            fs.unlinkSync(oldUser.local.photo);
                        }
                        res.redirect('/profile');
                    });
                });
        } else {
            console.log('no photo');
            User.findByIdAndUpdate(req.user.id, { $set: { "local.name": req.body.name, "local.age": req.body.age } }, { new: true }, function(err, updatedUser) {
                if (err) return handleError(err);
                req.flash('uploadMessage', 'Your info updated');
                res.redirect('/profile');
            });
        }
    });
};

// is banned middleware
function isBannedUser(req, res, next) {
    if (req.user.local.isBanned) res.render('banned.ejs', { user: req.user });
    else return next()
}

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

function perRoleRouter(req, res) {
    if (req.user.local.role === 'admin') {
        res.redirect('/admin');
    } else {
        res.redirect('/profile');
    }
}

function isSuperUser(req, res, next) {
    if (req.user.local.role == 'admin') {
        console.log('role - ' + req.user.local.role);
        return next();
    } else {
        console.log('role - ' + req.user.local.role);
        res.redirect('/profile')
            //return next();
    }
}