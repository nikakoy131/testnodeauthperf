// app/models/user.js
// load the things we need
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

// define the schema for our user model
const userSchema = mongoose.Schema({
    local: {
        email: String,
        password: String,
        age: { type: Number, min: 5, max: 150 },
        name: String,
        photo: String,
        role: String,
        isBanned: { type: Boolean, default: false }
    }
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    const SALT_ROUNDS = 12;
    return bcrypt.hashSync(password, bcrypt.genSaltSync(SALT_ROUNDS), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);