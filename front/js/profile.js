new Vue({
    el: '#vue-app',
    props: ['imageSrc', 'name', 'age'],
    data: {
        saveBut: false,
        oldAge: document.querySelector('#age').value,
        oldName: document.querySelector('#name').value
    },
    methods: {
        previewThumbnail: function(event) {
            var input = event.target;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var vm = this;
                reader.onload = function(e) {
                    vm.imageSrc = e.target.result;
                }
                reader.readAsDataURL(input.files[0]);

            }
        },
        validateChange: function() {
            if (this.oldAge === this.age && this.oldName === this.name && this.imageSrc === undefined) {
                this.saveBut = false;
            } else {
                this.saveBut = true;
            }
        }
    },
    mounted: function() {
        this.age = this.oldAge;
        this.name = this.oldName;
    },
    updated() {
        this.validateChange();
    }
})