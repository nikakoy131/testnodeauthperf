new Vue({
    el: "#vue-app",
    props: ['userlist'],
    data: {
        saveBut: false,
        changedUsers: [],
        message: ''
    },
    methods: {
        saveChanged: function(user) {
            this.message = '';
            console.log('user - ', user);
            if (this.changedUsers.indexOf(user) !== -1) {
                // remove user from array if it exist
                this.changedUsers.splice(this.changedUsers.indexOf(user), 1);
            } else {
                this.changedUsers.push(user);
            }
            if (this.changedUsers.length > 0) this.saveBut = true;
            else this.saveBut = false;
        },
        updateUsers: function() {
            this.$http.post('/users', { users: this.changedUsers })
                .then(response => {
                    console.log('body - ', response.body);
                    this.message = response.body.message;
                    this.saveBut = false;
                }, err => {
                    console.log('errror updating users - ', err);
                });
        }
    },
    mounted() {
        this.$http.get('/admin/users').then(response => {
            // get body data 
            this.userlist = response.body;
        }, response => {
            // error callback 
            console.log('error fetching data');
        });
    }
});