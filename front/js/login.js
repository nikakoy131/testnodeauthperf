new Vue({
    el: '#vue-app',
    data: {
        login: '',
        pass: '',
        message: '',
        loginBut: false
    },
    methods: {
        validateForm: function() {
            if (this.login.length < 1) {
                this.message = 'Login field is blank!';
                this.loginBut = false;
            } else if (this.pass.length < 1) {
                this.message = 'Password can`t be blank!';
                this.loginBut = false;
            } else {
                this.message = '';
                this.loginBut = true;
            }
        }
    },
    mounted: function() {
        this.message = this.$el.attributes['data-serv'].value;
    }
})