new Vue({
    el: '#vue-app',
    data: {
        pass1: '',
        pass2: '',
        login: '',
        signUpBut: false,
        message: ''
    },
    methods: {
        passValidate: function() {
            if (this.pass1 !== this.pass2) {
                this.message = 'Passwords mismatch!';
                this.signUpBut = false;
            } else if (this.pass1.length < 1) {
                this.message = 'Password can`t be blank!';
                this.signUpBut = false;
            } else {
                this.message = '';
                this.signUpBut = true;
            }

        },
        loginValidate: function() {
            if (this.login.length < 1) {
                this.message = 'Login field is blank!';
                this.signUpBut = false;
            } else {
                this.passValidate();
            }
        }
    },
    mounted: function() {
        this.message = this.$el.attributes['data-serv'].value;
    }
})