# INSTALLATION
First you need to get source code (if login and password promted, leave it blank).

```bash
git clone https://bitbucket.org/nikakoy131/testnodeauthperf.git
cd testnodeauthperf
```
To install dependencies just type
```bash
npm i
```
After this, you can start server. At now only dev mode supported.
```bash
npm run dev
```
After, just go to http://localhost:3001 in your favorite browser
# NOTE! 
1. You need node version 8 or higher
2. You need local mongodb installed or change config file app/config/database.js
3. First created user will be superuser (admin)
# PS
Этот код совсем не годиться для продакшена. Было сделано много упрощений. 
###### Например:
1. Было бы правильно не отправлять с бекенда пароль пользователя на фронт.
2. Также все странички сделаны в разном стиле. Login, profile, signup - почти SSR, admin - SPA.
3. Весьма полезно было бы все-таки собирать бандлы с webpack и использовать минифицированые версии пакетов.
4. Возможно стоило организовать хранение фото профилей в БД.
Но поскольку это всего лишь презентация, пусть все остается как есть.
